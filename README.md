Калькулятор на языке Java

Функционал:
* Бинарные функции: +, -, ×, ÷ .
* Унарные функции: - (минус).
* Произвольная вложенность скобок.

Не реализовано:
* Поддержка ввода дробных чисел.
* Тернарный оператор (например: 10 > 5 ? 1 : 2).