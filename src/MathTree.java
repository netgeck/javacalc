import java.util.List;


public class MathTree {
    private static Node nodeFromSyntax(Syntax syntax) {
        Node node = new Node();
        node.setData(syntax.token);

        switch (syntax.type) {
            case Digit:
                node.setKey(exprType.number);
                break;
            case operation:
                switch (syntax.token.charAt(0)) {
                    case '+':   node.setKey(exprType.plus);           break;
                    case '-':   node.setKey(exprType.minus);          break;
                    case '*':   node.setKey(exprType.multiplication); break;
                    case '/':   node.setKey(exprType.division);       break;
                }
                break;
            case brace:
                node.setKey(exprType.parentheses);
                break;
        }

        System.out.println("Created node: " + node.toString() + ": [" + node.getKey() + "] " + node.getData());
        return node;
    }


    public static Node parseTree(List<Syntax> tokens) {
        System.out.print("parseTree for expression: ");
        for (Syntax obj : tokens) {
            System.out.print(obj.token + " ");
        }
        System.out.println("\n");

        Node lastNode = null;

        for (int i = 0; i < tokens.size(); i++) {
            Syntax synt = tokens.get(i);
            System.out.println("\nProcessing syntax " + synt  + ": [" + synt.type + "] " + synt.token);

            Node newNode = nodeFromSyntax(synt);

            switch (synt.type) {
                case Digit:
                    if (lastNode != null) {
                        System.out.println("case Digit:");
                        if (Node.isNumber(lastNode) || Node.isParentheses(lastNode)) {
                            System.out.println("Digit may be child only for operations!");
                            return null;
                        }
                        System.out.println("Add digit as child for " + lastNode.getData());
                        lastNode.addChild(newNode);
                    }
                    break;
                case brace:
                    System.out.println("case brace:");
                    int endParenthesesPos = MathExpression.findClosingParentheses(tokens, i);
                    if (endParenthesesPos < 0) {
                        System.out.println("Parentheses mismatch!");
                        return null;
                    }
                    Node parenthesesSubtree = parseTree(tokens.subList(i+1, endParenthesesPos));
                    newNode.addChild(parenthesesSubtree);
                    i = endParenthesesPos;
                    if (lastNode != null) {
                        lastNode.addChild(newNode);
                    }
                    break;
                case operation:
                    System.out.println("case operation:");
                    if (lastNode == null || Node.isOperation(lastNode)) {
                        if (newNode.getKey() == exprType.minus) {
                            newNode.addChild(exprType.number, "0");
                            if (lastNode != null) {
                                lastNode.addChild(newNode);
                            }
                        } else {
                            System.out.println("Error: Operation again!");
                            return null;
                        }
                    } else {
                        for (Node inode = lastNode; inode != null; inode = inode.getParent()) {
                            System.out.println("\t" + inode + ": [" + inode.getKey() + "] " + inode.getData());

                            if (inode.isRoot()) {
                                System.out.println("Make new root for tree");
                                newNode.addChild(inode);
                                break;
                            }
                            if (inode.getParent().getPriority() < newNode.getPriority()) {
                                System.out.println("Insert between " + inode.getParent().getData() + " and " + inode.getData());
                                inode.InsertBeforeParent(newNode);
                                System.out.println(newNode);
                                System.out.println(newNode.getParent());
                                System.out.println(newNode.getLeft());
                                //System.out.println(newNode.getRight());
                                break;
                            }
                        }
                    }
                    break;
                default:
                    return null;
            }

            if (lastNode != null && newNode.isFree()) {
                System.out.println("Not found parent for processing node!");
                return null;
            }
            lastNode = newNode;
        }

        return Node.getRoot(lastNode);
    }


    public static double Calculate(Node expressionTree) {
        double res = 0;

        // leaf
        if (Node.isNumber(expressionTree)) {
            return Double.parseDouble(expressionTree.getData());
        }

        switch(expressionTree.getKey()) {
            case plus:
                return Calculate(expressionTree.getLeft()) + Calculate(expressionTree.getRight());
            case minus:
                return Calculate(expressionTree.getLeft()) - Calculate(expressionTree.getRight());
            case multiplication:
                return Calculate(expressionTree.getLeft()) * Calculate(expressionTree.getRight());
            case division:
                return Calculate(expressionTree.getLeft()) / Calculate(expressionTree.getRight());
            case parentheses:
                return Calculate(expressionTree.getLeft());
        }

        return res;
    }
}
