import java.util.List;

public class Calculator {
    public static double Calculate(String expression) {

        List<String> tokens = MathExpression.tokenize(expression);
        if (tokens == null) {
            System.out.println("Can't parse expression");
            throw new RuntimeException();
        }
//        for (String obj : tokens) {
//            System.out.println(obj);
//        }

        List<Syntax> syntaxes = MathExpression.Syntaxize(tokens);
        for (Syntax obj : syntaxes) {
            System.out.println(obj.type.toString() + ": " + obj.token);
        }

        Node tree = MathTree.parseTree(syntaxes);
        if (tree == null) {
            System.out.println("Can't parse Tree");
            throw new RuntimeException();
        }

        double res = MathTree.Calculate(tree);
        return res;
    }
}
