import java.util.List;
import java.util.Vector;

enum SyntaxType { Digit, operation, brace };


class Syntax {
    public SyntaxType   type;
    public String       token;
};


class MathExpression {
    private static String parseDigit(String in, int pos) {
        String out = "";
        while (pos < in.length()) {
            if (!Character.isDigit(in.charAt(pos))) {
                break;
            }

            out += in.charAt(pos++);
        }

        return out;
    }


    public static List<String> tokenize(String expression) {
        List<String> tokens = new Vector<String>();

        for (int pos = 0; pos < expression.length();) {
            char ch = expression.charAt(pos);

            switch (ch) {
                case '+':
                case '-':
                case '*':
                case '/':
                case '(':
                case ')': {
                    String lexem = "";
                    lexem += ch;
                    tokens.add(lexem);
                    pos++;
                }
                break;
                case ' ':
                    pos++; // skip
                    break;
                default:
                    if (Character.isDigit(ch)) {
                        String lexem = parseDigit(expression, pos);
                        pos += lexem.length();
                        tokens.add(lexem);
                    } else {
                        System.out.println("Unexpected sybmol: " + ch);
                        return null;
                    }
                    break;
            }
        }
        return tokens;
    }


    public static List<Syntax> Syntaxize(List<String> tokens) {
        List<Syntax> syntaxes = new Vector<Syntax>();

        for (String str : tokens) {
            Syntax elm = new Syntax();
            elm.token = str;

            switch (str.charAt(0)) {
                case '+':
                case '-':
                case '*':
                case '/':
                    elm.type = SyntaxType.operation;
                    break;
                case '(':
                case ')':
                    elm.type = SyntaxType.brace;
                    break;
                default:
                    if (Character.isDigit(str.charAt(0))) {
                        elm.type = SyntaxType.Digit;
                    }
                    break;
            }

            syntaxes.add(elm);
        }
        return syntaxes;
    }


    public static int findClosingParentheses(List<Syntax> tokens, int startItem) {
        int nestingСounter = 0;
        for (int item = startItem; item < tokens.size(); item++) {
            if (tokens.get(item).type == SyntaxType.brace) {
                if (tokens.get(item).token.equals("(")) {
                    ++nestingСounter;
                } else {
                    --nestingСounter;
                }

                if (nestingСounter == 0) {
                    return item;
                }
            }
        }
        return -1;
    }
}