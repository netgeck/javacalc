import javax.swing.*;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                CalcGui gui = new CalcGui();
                gui.setVisible(true);
            }
        });

//        {
//            String expr = "2+3+4*3";        // 17
//            double res = Calculator.Calculate(expr);
//            System.out.println("Expression: " + expr + " = " + res);
//        }
//        {
//            String expr = "2+3+4*3+1";      // 18
//            double res = Calculator.Calculate(expr);
//            System.out.println("Expression: " + expr + " = " + res);
//        }
//        {
//            String expr = "-1";             // -1
//            double res = Calculator.Calculate(expr);
//            System.out.println("Expression: " + expr + " = " + res);
//        }
//        {
//            String expr = "2 + -1";         // 1
//            double res = Calculator.Calculate(expr);
//            System.out.println("Expression: " + expr + " = " + res);
//        }
//        {
//            String expr = "2+(-1)";         // 1
//            double res = Calculator.Calculate(expr);
//            System.out.println("Expression: " + expr + " = " + res);
//        }
//        {
//            String expr = "2+(3+4)*3";      // 23
//            double res = Calculator.Calculate(expr);
//            System.out.println("Expression: " + expr + " = " + res);
//        }
//        {
//            String expr = "()";
//            double res = Calculator.Calculate(expr);
//            System.out.println("Expression: " + expr + " = " + res);
//        }
    }
}
