

enum exprType { number, plus, minus, multiplication, division, parentheses}


public class Node {
    private exprType key;
    private String  data;
    private Node left = null;
    private Node right = null;
    private Node parent = null;

    public void set(exprType key, String data) {
        this.setKey(key);
        this.setData(data);
    }

    public int getPriority() {
        switch (getKey()){
            case number:            return 0;
            case plus:              return 1;
            case minus:             return 1;
            case multiplication:    return 2;
            case division:          return 2;
            case parentheses:       return 3;
            default:
                return -1;
        }
    }

    public Boolean isFree() {
        return parent == null && left == null && right == null;
    }

    public void InsertBeforeParent(Node node) {
        if (!node.isFree()) {
            System.out.println("Onlyt free node allowed");
            return;
        }

        Node parent = this.getParent();
        if (parent.left == this) {
            parent.left = node;
        } else {
            parent.right = node;
        }

        node.parent = this.parent;
        this.parent = node;
        node.left = this;

    }

    public static Boolean isNumber(Node node) {
        return node.getKey() == exprType.number;
    }

    public static Boolean isOperation(Node node) {
        return node.getKey() == exprType.plus ||
                node.getKey() == exprType.minus ||
                node.getKey() == exprType.multiplication ||
                node.getKey() == exprType.division;
    }

    public static Boolean isParentheses(Node node) {
        return node.getKey() == exprType.parentheses;
    }

    public Boolean isRoot() {
        return parent == null;
    }

    public static Node getRoot(Node node) {
        while(!node.isRoot()) {
            node = node.parent;
        }
        return node;
    }

    public Node addParent() {
        Node newParent = new Node();
        newParent.left = this;

        if (this.parent == null) {
            this.parent = newParent;
            return newParent;
        }

        if (this.parent.left == this) {
            this.parent.left = newParent;
        } else {
            this.parent.right = newParent;
        }

        return newParent;
    }

    public Node addChild(Node node) {
        if (node.parent != null) {
            return null;
        }

        if (this.left == null) {
            left = node;
        } else if (right == null) {
            right = node;
        } else {
            return null;
        }

        node.parent = this;
        return node;
    }

    public Node addChild(exprType key, String data) {
        Node node = new Node();
        node.setKey(key);
        node.setData(data);

        return addChild(node);
    }

    public exprType getKey() {
        return key;
    }

    public void setKey(exprType key) {
        this.key = key;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Node getParent() {
        return parent;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }
}


